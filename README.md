# Projekt: JFit (Fr/2, Kr)

Name & Praktikumstermin: Gian Saß, 5229336 (Fr/2, Kr)

## Inhaltsverzeichnis

* 1. [Kurzbeschreibung inkl. Screenshot](#Kurzbeschreibunginkl.Screenshot)
* 2. [Beschreibung des Projektaufbaus](#BeschreibungdesProjektaufbaus)
	* 2.1. [Abgabedateien (LOC)](#AbgabedateienLOC)
	* 2.2. [Testdateien (TST)](#TestdateienTST)
* 3. [Aufbau der Anwendung](#AufbauderAnwendung)
	* 3.1. [Quellcode-Struktur](#Quellcode-Struktur)
* 4. [Dokumentation des implementierten WebAPIs](#DokumentationdesimplementiertenWebAPIs)
	* 4.1. [Übersicht aller Routen](#bersichtallerRouten)
	* 4.2. [GET /](#GET)
	* 4.3. [POST /register](#POSTregister)
	* 4.4. [GET /login](#GETlogin)
	* 4.5. [POST /login](#POSTlogin)
	* 4.6. [GET /logout](#GETlogout)
	* 4.7. [GET /day](#GETday)
	* 4.8. [GET /add_food](#GETadd_food)
	* 4.9. [POST /add_food](#POSTadd_food)
	* 4.10. [POST /remove_item](#POSTremove_item)
	* 4.11. [GET /suggest_item](#GETsuggest_item)
* 5. [Technischer Anspruch (TA) und Umsetzung der Features](#TechnischerAnspruchTAundUmsetzungderFeatures)
	* 5.1. [Validation](#Validation)
	* 5.2. [AccessManager und CookieStore](#AccessManagerundCookieStore)
	* 5.3. [Views and Templates](#ViewsandTemplates)
* 6. [User Guide](#UserGuide)
	* 6.1. [Accounterstellung](#Accounterstellung)
	* 6.2. [Tagesübersicht](#Tagesbersicht)
	* 6.3. [Lebensmittel eintragen](#Lebensmitteleintragen)
	* 6.4. [Ergänzung](#Ergnzung)
* 7. [Quellennachweis](#Quellennachweis)
	* 7.1. [Libraries](#Libraries)

##  1. <a name='Kurzbeschreibunginkl.Screenshot'></a>Kurzbeschreibung inkl. Screenshot

>JFit ist ein simpler Kalorien-Tracker. Er erlaubt die Erstellung täglicher Ernährungs-Logs, in denen jeweils die erreichte Anzahl von Kalorien berechnet wird.

![Front](front.png)
![Day](day.png)

**Hinweise**: keine

##  2. <a name='BeschreibungdesProjektaufbaus'></a>Beschreibung des Projektaufbaus

###  2.1. <a name='AbgabedateienLOC'></a>Abgabedateien (LOC)

Verlinkter Dateiname | Dateiart | LOC
---------------------|----------|-----
[App.java](/src/main/java/de/thm/mni/pis/aufgabe5/App.java)|Java|35
[ControllerUtil.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/ControllerUtil.java)|Java|8
[DatabaseObject.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/DatabaseObject.java)|Java|4
[Day.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/Day.java)|Java|69
[DayController.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/DayController.java)|Java|66
[DayDAO.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/DayDAO.java)|Java|5
[DayResult.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/DayResult.java)|Java|6
[DayService.java](/src/main/java/de/thm/mni/pis/aufgabe5/services/DayService.java)|Java|47
[EnergyExpenditure.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/EnergyExpenditure.java)|Java|9
[Flash.java](/src/main/java/de/thm/mni/pis/aufgabe5/Flash.java)|Java|8
[Food.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/Food.java)|Java|35
[MemoryDatabase.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/MemoryDatabase.java)|Java|32
[Routes.java](/src/main/java/de/thm/mni/pis/aufgabe5/Routes.java)|Java|13
[SiteController.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/SiteController.java)|Java|12
[User.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/User.java)|Java|11
[UserController.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/UserController.java)|Java|46
[UserDAO.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/UserDAO.java)|Java|5
[UserRole.java](/src/main/java/de/thm/mni/pis/aufgabe5/UserRole.java)|Java|6
[UserService.java](/src/main/java/de/thm/mni/pis/aufgabe5/services/UserService.java)|Java|22
[ViewModel.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/ViewModel.java)|Java|28
[addFood.vm](/src/main/resources/views/day/addFood.vm)|Velocity Template|33
[index.vm](/src/main/resources/views/site/index.vm)|Velocity Template|40
[layout.vm](/src/main/resources/views/layout.vm)|Velocity Template|36
[login.vm](/src/main/resources/views/user/login.vm)|Velocity Template|20
[script.js](/src/main/resources/public/script.js)|JavaScript|34
[show.vm](/src/main/resources/views/day/show.vm)|Velocity Template|62
[style.css](/src/main/resources/public/style.css)|CSS|84
Gesamt||776

###  2.2. <a name='TestdateienTST'></a>Testdateien (TST)

Verlinkter Dateiname | Testart | Anzahl der Tests
---------------------|---------|-----------------
[DatabaseTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/models/DatabaseTest.java)|JUnit4|7
[DayServiceTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/services/DayServiceTest.java)|JUnit4|6
[DayTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/models/DayTest.java)|JUnit4|6
[FoodTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/models/FoodTest.java)|JUnit4|10
[UserTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/models/UserTest.java)|JUnit4|3
[UserServiceTest.java](/src/test/java/de/thm/mni/pis/aufgabe5/services/UserServiceTest.java)|JUnit4|9
Gesamt||41

Die Tests werden wie folgt ausgeführt:

>gradle test

##  3. <a name='AufbauderAnwendung'></a>Aufbau der Anwendung

Das Projekt orientiert sich an dem MVC-Paradigma (Model-View-Controller) mit einem zusätzlichen Service-Layer und ist dementsprechend geschichtet.

![Backend](structure.png)

In seiner Architektur ist das Projekt in vier Ebenen geschichtet:
1. Controller - Empfängt und beantwortet HTTP-Requests als Handler für Javalin.
2. Service - Führt Anwendungslogik aus und ist von Javalin komplett entkoppelt.
3. Model - Datenbank und Business-Logic
4. View - Frontend bestehend aus Velocity-Templates

Die Pfeile in der Grafik deutet auf den Datenfluss.

###  3.1. <a name='Quellcode-Struktur'></a>Quellcode-Struktur

Der Quellcode ist wie folgt strukturiert:

~~~
src
└───main
    └───de.mni.thm.pis.aufgabe5
    |   └───controllers
    |   |   |───ControllerUtil.java
    |   |   |───DayController.java
    |   |   |───SiteController.java
    |   |   |───UserController.java
    |   └───models
    |   |   |───DatabaseObject.java
    |   |   |───Day.java
    |   |   |───DayDAO.java
    |   |   |───DayResult.java
    |   |   |───EnergyExpenditure.java
    |   |   |───Exercise.java
    |   |   |───Food.java
    |   |   |───MemoryDatabase.java
    |   |   |───User.java
    |   |   |───UserDAO.java
    |   |   |───ViewModel.java
    |   └───services
    |   |   |───DayService.java
    |   |   |───UserService.java
    |   |───App.java
    |   |───Flash.java
    |   |───Routes.java
    |   |───UserRole.java
    └───resources
        └───public
        |   |───script.js
        |   |───style.css
        └───views
            └───day
            |   |───addFood.vm
            |   |───show.vm
            └───site
            |   |───index.vm
            └───user
            |   |───login.vm
            |───layout.vm
~~~

##  4. <a name='DokumentationdesimplementiertenWebAPIs'></a>Dokumentation des implementierten WebAPIs

###  4.1. <a name='bersichtallerRouten'></a>Übersicht aller Routen

| Route  | Method| Controller | Authenticated\* |
|--------|-------|------------|-----------------|
|/       |GET    |SiteController|No             |
|/register|POST  |UserController|No             |
|/login  |GET    |UserController|No             |
|/login  |POST   |UserController|Yes            |
|/logout |GET    |UserController|Yes            |
|/day    |GET    |DayController |Yes            |
|/add_food|GET   |DayController |Yes            |
|/add_food|POST  |DayController |Yes            |
|/remove_item|POST|DayController|Yes            |
|/suggest_item|GET|DayController|Yes            |

\* Ein Login-Cookie muss gesetzt sein. Ist keiner gesetzt, leitet die Anfrage auf `/` um

###  4.2. <a name='GET'></a>GET /

**Parameter:** keine

**Antwort:** (Status=200, Content-Type=text/html)

Stellt die Landing-Page dar.

###  4.3. <a name='POSTregister'></a>POST /register

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:email | No      | E-Mail  |
| String:password | No      | Passwort  |

**Erfolgreiche Antwort:** (Status=302, Location=/day, Set-Cookie: currentUser=userId)

**Nicht erfolgreiche Registration:** (Status=302, Location=/day)

**Invalide Form-Parameter:** (Status=400)

Registriert einen neuen Account.

###  4.4. <a name='GETlogin'></a>GET /login

**Parameter:** keine

**Antwort:** (Status=200, Content-Type=text/html)

Stellt die Login-Seite dar.

###  4.5. <a name='POSTlogin'></a>POST /login

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:email | No      | E-Mail  |
| String:password | No      | Passwort  |

**Erfolgreiche Antwort:** (Status=302, Location=/day, Set-Cookie: currentUser=userId)

**Nicht erfolgreiche Anmeldung:** (Status=302, Location=/login)

**Invalide Form-Parameter:** (Status=400)

Meldet sich in einem bereits existierenden Account ein.

###  4.6. <a name='GETlogout'></a>GET /logout

**Parameter:** keine

**Antwort:** (Status=302, Location=/, Set-Cookie: "")

Abmeldung.

###  4.7. <a name='GETday'></a>GET /day

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:date | Yes      | Das Datum des Tages im Format yyyy-MM-dd |

**Antwort:** (Status=200, Content-Type=text/html)

Zeigt die Tagesübersicht für das jeweilige Datum bzw. heutige Datum an.

###  4.8. <a name='GETadd_food'></a>GET /add_food

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:date | Yes      | Das Datum des Tages im Format yyyy-MM-dd |

**Antwort:** (Status=200, Content-Type=text/html)

Zeigt das Formular zum Hinzufügen eines neuen Lebensmittels an.

###  4.9. <a name='POSTadd_food'></a>POST /add_food

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:name | No      | Name des Lebensmittels |
| String:unit | No      | Einheit des Lebensmittels |
| int:amount | No      | Menge des Lebensmittels |
| int:avgCalories | No      | Durchschnittskalorien |
| int:day | No      | ID des Tages |

**Erfolgreiche Antwort:** (Status=302, Location=/day?date=day.date)

**Nicht erfolgreich:** (Status=302, Location=/add_food?date=date)

**Invalide Form-Parameter:** (Status=400)

Fügt ein neues Lebensmittel zu einem existierenden Tag hinzu.

###  4.10. <a name='POSTremove_item'></a>POST /remove_item

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| int:dayId | No      | ID des Tages |
| int:idx | No      | Index des Lebensmittels |

**Erfolgreiche Antwort:** (Status=302, Location=/day?date=day.date)

**Nicht erfolgreich:** (Status=400)

**Invalide Form-Parameter:** (Status=400)

Entfernt ein Lebensmittel aus einem Tag.

###  4.11. <a name='GETsuggest_item'></a>GET /suggest_item

**Parameter:**

| Name | Optional | Purpose |
|------|----------|---------|
| String:query | No      | Das Lebensmittel nach dem gesucht werden soll. |

**Erfolgreiche Antwort:** (Status=200, Content-Type=application/json)

**Nicht erfolgreich:** (Status=404)

**Invalide Form-Parameter:** (Status=400)

Findet ein Lebensmittel mit dem Namen `query` und gibt dies per JSON zurück.

```
{
    "name": "string",
    "unit": "string",
    "avgCalories": "int",
    "amount": "int"
}
```

##  5. <a name='TechnischerAnspruchTAundUmsetzungderFeatures'></a>Technischer Anspruch (TA) und Umsetzung der Features

Ich habe folgende Features verwendet. Die verlinkte Datei zeigt beispielhaft den Einsatz dieses Features in den angegebenen Zeilen im Quellcode.

1. Validation, [DayController.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/DayController.java) (47-51)
2. AccessManager, [App.java](/src/main/java/de/thm/mni/pis/aufgabe5/App.java) (18-27)
3. CookieStore, [UserController.java](/src/main/java/de/thm/mni/pis/aufgabe5/controllers/UserController.java) (43)
4. Views and Templates, [layout.vm](/src/main/resources/views/layout.vm) (1-39)

###  5.1. <a name='Validation'></a>Validation

In Controllern wird Javalins Validator durch Parameter-Methoden benutzt.

```java
final var email = ctx.formParam("email", String.class).get();
final var password = ctx.formParam("password", String.class).get();
final var calorieGoal = ctx.formParam("calorieGoal", Integer.class).check(i -> i > 0).get();
```

Damit wird sichergestellt, dass Parameter typsicher sind.

###  5.2. <a name='AccessManagerundCookieStore'></a>AccessManager und CookieStore

Zur Verwendung von JFit muss der Benutzer einen Account erstellen.
Aufgrund Beschränkungen in dieser Projektarbeit wird hier kein Auge auf Sicherheit geworfen.
So werden Passwörter und Cookies nicht verschlüsselt.


Um zwischen eingeloggten und nicht-eingeloggten Usern zu unterscheidern, wird der `AccessManager` von Javalin eingesetzt.


Jede Route wird mit Benutzer-Rollen (UserRole) assoziiert (eingeloggt und nicht eingeloggt).

```java
app.routes(() -> {
    get(Routes.SITE_INDEX, SiteController::index, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

    get(Routes.DAY_SHOW, DayController::show, roles(UserRole.LOGGED_IN));

    post(Routes.USER_REGISTER_POST, UserController::createAccount, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

    get(Routes.USER_LOGIN, UserController::login, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));
    post(Routes.USER_LOGIN_POST, UserController::loginAccount, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

    get(Routes.USER_LOGOUT, UserController::logout, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

    get(Routes.DAY_ADD_FOOD, DayController::addFood, roles(UserRole.LOGGED_IN));
    post(Routes.DAY_ADD_FOOD_POST, DayController::createFood, roles(UserRole.LOGGED_IN));

    get(Routes.DAY_REMOVE_ITEM, DayController::removeItem, roles(UserRole.LOGGED_IN));

    get(Routes.DAY_SUGGEST_ITEM, DayController::suggestItem, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));
});
```

Der AccessManager prüft, ob der Login-Cookie gesetzt ist und ob der entsprechende Benutzer auch existiert.

```java
app.accessManager((handler, ctx, permittedRoles) -> {
    UserRole userRole = ViewModel.getCurrentUser(ctx).isPresent() ? UserRole.LOGGED_IN : UserRole.NOT_LOGGED_IN;

    if (permittedRoles.contains(userRole)) {
        handler.handle(ctx);
    } else {
        ControllerUtil.setFlash(ctx, "danger", "Sie sind nicht eingeloggt!");
        ctx.redirect(Routes.SITE_INDEX);
    }
});
```

Der Login-Cookie wird in den Controllern und in [ViewModel.java](/src/main/java/de/thm/mni/pis/aufgabe5/models/ViewModel.java) benutzt.

```java
/**
 * Retrieves the current user of the current session
 *
 * @param ctx the Javalin context
 * @return the user
 */
public static Optional<User> getCurrentUser(Context ctx) {
    final int userID = Integer.valueOf(ctx.cookieStore("currentUser") != null ? ctx.cookieStore("currentUser") : -1);
    return UserDAO.instance.findBy(u -> u.getId() == userID);
}
```

###  5.3. <a name='ViewsandTemplates'></a>Views and Templates

Das Frontend besteht aus mehreren View-Templates, geschrieben in der `Velocity Markup Language`, welche Javalin standartmäßig unterstützt.
Dazu gehört noch ein statischer Anteil, der aus eine Javascript- und einer CSS-Datei besteht.

Als Framework wurde Bootstrap 4 gewählt. Zusätzlich wird die Google Charts API benutzt.

```
src
└───main
    └───resources
        └───public
            |───script.js
            |───style.css
        └───views
            └───day
                |───addFood.vm
                |───show.vm
            └───site
                |───index.vm
            └───user
                |───login.vm
            |───layout.vm
```

Die Namen der Templates sind an den jeweiligen Controller-Aktionen orientiert (z.B. `DayController::addFood -> views/day/addFood.vm`).
Alle Templates basieren auf einem sogenanntes Layout (`layout.vm`). Layouts erlauben die Wiederverwendung von HTML-Code, indem
das Gerüst im Layout gebaut wird und der Inhalt dem einzelnen Template überlassen wird. 

Beispiel:

```
#parse("/views/layout.vm")
#@mainLayout()

<h1>Content</h1>

#end

```

**View-Models**

Informationen vom Backend werden an das Frontend durch ein sogenanntes `View-Model` transferiert. Dies ist Bestandteil
von fast allen Template-Engines. Javalin realisiert dies durch eine `Map<String, Object>`, welche beim Aufruf von
`ctx.render` angegeben wird. Dabei bilden die Keys die View-Variabeln und die Values deren Werte.

Durch das ViewModel können sogar Klassen-Instanzen übergeben werden, deren Methoden im Template aufgerufen werden
können (siehe `day.vm:20`).

Die Klasse `models.ViewModel` stellt den Controllern statische Methoden zur Erstellung von ViewModels bereit.

##  6. <a name='UserGuide'></a>User Guide

###  6.1. <a name='Accounterstellung'></a>Accounterstellung
Nachdem Öffnen der App wird der Benutzer mit einer Landing Page begrüßt. Hier wird
der Benutzer dazu gebeten, einen Account zu erstellen. Zum Erstellen wird eine
E-Mail, ein Passwort und ein tägliches Kalorienziel verlangt. Nach dem Registrieren
oder Einloggen wird der User sofort auf die Tagesübersicht des heutigen Datums weitergeleitet.

###  6.2. <a name='Tagesbersicht'></a>Tagesübersicht
Auf der Tagesübersicht kann der Benutzer Lebensmittel zu den Energiequellen hinzufügen.
Weiterhin gibt es eine Übersicht der bisherigen eingenommenen Lebensmittel und der jetzige
Kalorienstand. Es existieren Pfeile neben dem Datum, die es erlauben, zwischen dem Datum von
gestern und morgen zu navigieren.

Mit dem Button "Essen hinzufügen" kann der Benutzer ein neues Lebensmittel eingeben. Bereits
eingetragene Lebensmittel werden unter "Energieaufwand" gelistet. Dort kann der Benutzer
sie auch vom Tag wieder entfernen.

Die eingenommenen Kalorien werden summiert und letztlich mit dem Tagesziel verglichen.
Ein Kuchen-Diagramm stellt auch noch einmal statistisch da, wieviele Kalorien bereits
eingenommen und noch zu einzunehmen sind.

###  6.3. <a name='Lebensmitteleintragen'></a>Lebensmittel eintragen
Es werden insgesamt vier Werte beim Eintragen eines Lebensmittels abgefragt.
Dazu zählt Name, Einheit der Messung, Durchschnittliche Kalorien pro hundert dieser Einheit (z.B. wenn Gramm, dann wieviele Kalorien pro 100g, da dies meistens auf Verpackungen steht.)
und letztendlich die Menge.

Die resultierenden Kalorien werden berechnet, in dem die Menge (M) mit den Kalorien pro 100 (K100) in dieser Weise multipliziert werden:
```
K = M * K100 / 100U

U = Einheit
```

Beispiel: Haferflocken mit 300 Kalorien pro 100g und einer Menge von 50g.
Dann ist M = 50g und K100 = 300. Dann gilt:

```
K = 50g * 300kcal / 100g = 150kcal
```

###  6.4. <a name='Ergnzung'></a>Ergänzung
Beim Eintragen des Namens hilft eine Ergänzungsfunktion.
Nach jedem Input-Event, wird ein GET-Request an den Endpoint `/suggest_item?q=...` gesendet. Im Backend
wird dort nach einem Lebensmittel gesucht, dass dem Namen gleicht. Ist dies erfolgreich, dann
wird ein JSON-Objekt zurückgesendet und die Eingabe-Form mit diesen Informationen ergänzt.

##  7. <a name='Quellennachweis'></a>Quellennachweis

Sehr oft wurde die [Javalin-Dokumentation](https://javalin.io/documentation) zur Hilfe gerufen. Der Code orientiert sich an
einem [Projekt-Beispiel von Javalin](https://javalin.io/tutorials/website-example).

Beim CSS-Code wird beim Button-Design außerdem ein Design [aus codepen.io](https://codepen.io/pirrera/pen/bqVeGx) benutzt.

Die Graphik, die das Backend visualisiert, wurde von mir mithilfe von Adobe Illustrator selber erstellt.

Es wurde auf ein [Tutorial von Google](https://google-developers.appspot.com/chart/interactive/docs/gallery/piechart) zugegriffen.

###  7.1. <a name='Libraries'></a>Libraries
Folgende Libraries wurden im Projekt verwendet:

| Name | Version | Verwendung |
|------|---------|------------|
|Javalin|2.8.0   |Web framework|
|slf4j-simple|1.7.26   |Logging-Framework für Javalin|
|velocity-engine-core|2.0|Template-Engine|
|JUnit|4.12|Unit-Test-Framework|
|Lombok|1.18.8|Code-Generator für Getter/Setter, Builder-Pattern, etc.|
|Bootstrap|4|Frontend-Framework|
|Google Charts||JavaScript-Library für das Zeichnen von Graphen/Charts|

