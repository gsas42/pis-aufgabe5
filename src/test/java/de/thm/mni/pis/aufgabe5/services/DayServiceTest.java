package de.thm.mni.pis.aufgabe5.services;

import de.thm.mni.pis.aufgabe5.services.*;
import de.thm.mni.pis.aufgabe5.models.*;

import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import static org.junit.Assert.*;

public class DayServiceTest {
    private User currentUser;

    @Before
    public void setup() {
        DayDAO.instance.clear();
        UserDAO.instance.clear();

        currentUser = UserService.createAccount("foo@bar.com", "password123", 2000);
    }

    @Test
    public void testNewDayCreation() {
        final var day = DayService.getOrCreateDay(new Date(), currentUser);

        assertThat(day, notNullValue());
    }

    @Test
    public void testGetPreviousDay() {
        final var date = new Date();

        final var before = DayService.getOrCreateDay(date, currentUser);
        final var after = DayService.getOrCreateDay(date, currentUser);

        assertThat(before, notNullValue());
        assertThat(after, notNullValue());

        assertThat(before, is(after));
    }

    @Test
    public void testFoodCreation() {
        final var day = DayService.getOrCreateDay(new Date(), currentUser);

        DayService.createFood(day.getId(), "Cheeseburger", "g", 250, 200);

        assertTrue(day.getExpenditures().size() == 1);
    }

    @Test
    public void testRemoveItem() {
        final var day = DayService.getOrCreateDay(new Date(), currentUser);
        DayService.createFood(day.getId(), "Cheeseburger", "g", 250, 200);

        DayService.removeItem(day, 0);
        assertTrue(day.getExpenditures().size() == 0);
    }

    @Test
    public void testFindSimilarItem() {
        final var day = DayService.getOrCreateDay(new Date(), currentUser);
        DayService.createFood(day.getId(), "Cheeseburger", "g", 250, 200);

        final var item = DayService.findSimilarItem("Cheeseburger");

        assertTrue(item.isPresent());
        assertThat(item.get(), is(day.getExpenditures().get(0)));
    }

    @Test
    public void testFindNoSimilarItem() {
        final var day = DayService.getOrCreateDay(new Date(), currentUser);
        DayService.createFood(day.getId(), "Cheeseburger", "g", 250, 200);

        final var item = DayService.findSimilarItem("Pizza");
        assertFalse(item.isPresent());
    }
}
