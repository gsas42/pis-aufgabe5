package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class FoodTest {
    @Test
    public void testConstructor() {
        Food burger = new Food("Cheeseburger", "g", 250, 200);
        assertThat(burger, notNullValue());
    }

    @Test
    public void testThatCaloriesAreCalculatedCorrectly() {
        Food burger = new Food("Cheeseburger", "g", 250, 200);

        assertThat(burger.getCalories(), is(250 * 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCannotUseNegativeCalories() {
        Food burger = new Food("Cheeseburger", "g", -250, 200);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCannotUseNegativeAmount() {
        Food burger = new Food("Cheeseburger", "g", 250, -200);
    }

    @Test
    public void testDescription() {
        Food burger = new Food("Cheeseburger", "g", 250, 200);

        assertThat(burger.getDescription(), is("Cheeseburger (250 kcal pro 100g - 200g)"));
    }

    @Test
    public void testJSON() {
        Food burger = new Food("Cheeseburger", "g", 250, 200);

        assertThat(burger.toJSON(), is("{ \"name\": \"Cheeseburger\", \"unit\": \"g\", \"amount\": 200, \"avgCalories\": 250 }"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameCannotBeNull() {
        Food burger = new Food(null, "g", 250, 200);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameCannotEmpty() {
        Food burger = new Food("", "g", 250, 200);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnitCannotBeNull() {
        Food burger = new Food("Cheeseburger", null, 250, 200);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnitCannotEmpty() {
        Food burger = new Food("Cheeseburger", "", 250, 200);
    }
}
