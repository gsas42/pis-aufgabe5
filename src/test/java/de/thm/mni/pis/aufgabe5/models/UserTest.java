package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import static org.junit.Assert.*;

public class UserTest {
    @Test
    public void testBuild() {
        final var u = User.builder()
            .email("foo@bar.com")
            .password("test123")
            .build();

        assertThat(u.getEmail(), is("foo@bar.com"));
        assertThat(u.getPassword(), is("test123"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullEmail() {
        final var u = User.builder()
            .email(null)
            .password("test123")
            .build();
    }

    @Test(expected = NullPointerException.class)
    public void testNullPassword() {
        final var u = User.builder()
            .email("foo@bar.com")
            .password(null)
            .build();
    }
}


