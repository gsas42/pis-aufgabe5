package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;
import static org.hamcrest.CoreMatchers.is;
import org.junit.*;
import static org.junit.Assert.*;

public class DatabaseTest {
    class TestObject implements DatabaseObject {
        public int id = 0;
        public int x = 0;

        public TestObject(int id, int x) {
            this.id = id;
            this.x = x;
        }

        public int getId() { return id; }
        public int getX() { return x; }
    }

    private MemoryDatabase<TestObject> db = new MemoryDatabase<>();

    @Before
    public void setup() {
        db.clear();
    }

    @Test
    public void testIDIncrements() {
        assertThat(db.nextId(), is(0));
        assertThat(db.nextId(), is(1));
        assertThat(db.nextId(), is(2));
    }

    @Test
    public void testInsertion() {
        final var obj = new TestObject(db.nextId(), 5);

        final var returnValue = db.insert(obj);
        assertThat(returnValue, is(obj));
    }

    @Test
    public void testForEach() {
        final var obj1 = new TestObject(db.nextId(), 5);
        final var obj2 = new TestObject(db.nextId(), 10);

        db.insert(obj1);
        db.insert(obj2);

        final var list = new ArrayList<TestObject>();

        db.forEach(obj -> {
            list.add(obj);
        });

        assertThat(list.size(), is(2));
    }

    @Test
    public void testFindBy() {
        final var obj1 = new TestObject(db.nextId(), 5);
        final var obj2 = new TestObject(db.nextId(), 10);

        db.insert(obj1);
        db.insert(obj2);

        final var result = db.findBy(obj -> obj.getX() == 5);

        assertTrue(result.isPresent());
        assertThat(result.get(), is(obj1));
    }

    @Test
    public void testFindByNonExisting() {
        final var obj1 = new TestObject(db.nextId(), 5);
        final var obj2 = new TestObject(db.nextId(), 10);

        db.insert(obj1);
        db.insert(obj2);

        final var result = db.findBy(obj -> obj.getX() == 42);

        assertFalse(result.isPresent());
    }

    @Test
    public void testClearID() {
        final var id = db.nextId();
        db.clear();

        final var id2 = db.nextId();

        assertThat(id, is(id2));
    }

    @Test
    public void testClearObjects() {
        db.insert(new TestObject(db.nextId(), 100));

        assertTrue(db.findBy(obj -> obj.getX() == 100).isPresent());

        db.clear();

        assertFalse(db.findBy(obj -> obj.getX() == 100).isPresent());
    }
}
