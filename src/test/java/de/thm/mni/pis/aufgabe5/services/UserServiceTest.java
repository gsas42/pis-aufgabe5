package de.thm.mni.pis.aufgabe5.services;

import de.thm.mni.pis.aufgabe5.services.*;
import de.thm.mni.pis.aufgabe5.models.*;

import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import static org.junit.Assert.*;

public class UserServiceTest {
    @Before
    public void setup() {
        UserDAO.instance.clear();
    }

    @Test
    public void testAccountCreation() {
        final var u = UserService.createAccount("foo@bar.com", "password123", 2000);

        assertThat(u, notNullValue());
    }

    @Test
    public void testCannotUseEmailTwice() {
        final var first = UserService.createAccount("foo@bar.com", "password123", 2000);
        final var second = UserService.createAccount("foo@bar.com", "password123", 2000);

        assertThat(second, nullValue());
    }

    @Test
    public void testCannotUseNullEmail() {
        final var u = UserService.createAccount(null, "password123", 2000);

        assertThat(u, nullValue());
    }

    @Test
    public void testCannotUseEmptyEmail() {
        final var u = UserService.createAccount("", "password123", 2000);

        assertThat(u, nullValue());
    }

    @Test
    public void testCannotUseNullPassword() {
        final var u = UserService.createAccount("foo@bar.com", null, 2000);

        assertThat(u, nullValue());
    }

    @Test
    public void testCannotUseEmptyPassword() {
        final var u = UserService.createAccount("foo@bar.com", "", 2000);

        assertThat(u, nullValue());
    }

    @Test
    public void testLogin() {
        final var u = UserService.createAccount("foo@bar.com", "password123", 2000);
        final var login = UserService.loginAccount("foo@bar.com", "password123");

        assertTrue(login.isPresent());
        assertThat(login.get(), is(u));
    }

    @Test
    public void testLoginFalseCredentials() {
        final var u = UserService.createAccount("foo@bar.com", "wrongpassword123", 2000);
        final var login = UserService.loginAccount("foo@bar.com", "password123");

        assertFalse(login.isPresent());
    }

    @Test
    public void testLoginNonExistentAccount() {
        final var login = UserService.loginAccount("foo@bar.com", "password123");

        assertFalse(login.isPresent());
    }
}
