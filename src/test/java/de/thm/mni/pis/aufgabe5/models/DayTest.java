package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;
import static org.hamcrest.CoreMatchers.is;
import org.junit.*;
import static org.junit.Assert.*;
import java.time.*;
import java.text.*;

public class DayTest {
    @Before
    public void setup() {
        DayDAO.instance.clear();
    }

    @Test
    public void testThatTotalCaloriesAreCalculatedCorrectly() {
        final var d = Day.builder()
            .plannedCalories(2000)
            .build();
        final var burger = new Food("Cheeseburger", "g", 250, 200);

        d.addExpenditure(burger);
        assertThat(d.getTotalCalories(), is(500));
    }

    @Test
    public void testDayResult() {
        final var d = Day.builder()
            .plannedCalories(2000)
            .build();
        final var burger = new Food("Cheeseburger", "g", 250, 200);

        d.addExpenditure(burger);

        final var pizza = new Food("Pizza", "g", 1000, 200);
        d.addExpenditure(pizza);

        assertThat(d.getTotalCalories(), is(2500));
        assertThat(d.getResult(), is(DayResult.TOO_MANY_CALORIES));
    }

    @Test
    public void testThatRemainingCaloriesAreCalculatedCorrectly() {
        final var d = Day.builder()
            .plannedCalories(2000)
            .build();
        final var burger = new Food("Cheeseburger", "g", 250, 200);

        d.addExpenditure(burger);
        assertThat(d.getRemainingCalories(), is(1500));
    }

    @Test
    public void testDateString() throws ParseException {
        final var format = new SimpleDateFormat("yyyy-MM-dd");

        final var date = format.parse("2019-07-11");
        final var d = Day.builder()
            .plannedCalories(2000)
            .date(date)
            .build();

        assertThat(d.getDateString(), is("11.07.2019"));
    }

    @Test
    public void testPrevDate() throws ParseException {
        final var format = new SimpleDateFormat("yyyy-MM-dd");

        final var date = format.parse("2019-07-11");
        final var d = Day.builder()
            .plannedCalories(2000)
            .date(date)
            .build();

        assertThat(d.getPrevDate(), is("2019-07-10"));
    }

    @Test
    public void testNextDate() throws ParseException {
        final var format = new SimpleDateFormat("yyyy-MM-dd");

        final var date = format.parse("2019-07-11");
        final var d = Day.builder()
            .plannedCalories(2000)
            .date(date)
            .build();

        assertThat(d.getNextDate(), is("2019-07-12"));
    }
}
