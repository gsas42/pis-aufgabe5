function fillSuggestion() {
  const http = new XMLHttpRequest();

  http.open("GET", "/suggest_item?q="+document.getElementById("name").value);
  http.send();

  http.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(http.responseText);
      document.getElementById("unit").value = obj["unit"];
      document.getElementById("amount").value = obj["amount"];
      document.getElementById("avgCalories").value = obj["avgCalories"];
    }
  };
}

window.onload = function() {
  var piechart = document.getElementById('piechart');

  if(piechart !== null) {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = google.visualization.arrayToDataTable([
        ['Kalorien', 'Kalorien'],
        ['Eingenommene Kalorien', parseInt(piechart.getAttribute("data-calories"))],
        ['Verbleibende Kalorien', Math.max(parseInt(piechart.getAttribute("data-remaining-calories")), 0)],
      ]);

      var options = {
        backgroundColor: 'transparent',
        legend: 'none',
        colors: ['#00dd00', '#ccc']
      };

      var chart = new google.visualization.PieChart(piechart);

      chart.draw(data, options);
    }
  }
}
