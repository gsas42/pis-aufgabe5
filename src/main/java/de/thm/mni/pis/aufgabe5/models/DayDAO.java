package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;

/**
 * The Data Access Object responsible for managing the day database
 */
public class DayDAO extends MemoryDatabase<Day> {
    public static DayDAO instance = new DayDAO();
}
