package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.*;
import de.thm.mni.pis.aufgabe5.models.UserDAO;
import io.javalin.Context;
import java.util.*;
import java.util.function.*;

/**
 * Helper class for view models
 */
public class ViewModel {
    /**
     * Retrieves the current user of the current session
     *
     * @param ctx the Javalin context
     * @return the user
     */
    public static Optional<User> getCurrentUser(Context ctx) {
        final int userID = Integer.valueOf(ctx.cookieStore("currentUser") != null ? ctx.cookieStore("currentUser") : -1);
        return UserDAO.instance.findBy(u -> u.getId() == userID);
    }

    /**
     * Creates a common map used for passing information to the view
     *
     * It adds basic information, such as the current user, and manages flash information
     *
     * @param ctx the Javalin context
     * @return the view model
     */
    public static Map<String, Object> of(Context ctx) {
        final var map = new HashMap<String, Object>();
        getCurrentUser(ctx).ifPresent(u -> map.put("currentUser", u));

        map.put("siteTitle", "Aufgabe 5");

        /*
         * If a flash is set in the session attribute then add that to the view
         * model and remove it from session attributes
         */
        if(ctx.sessionAttribute("flash") != null) {
            map.put("flash", Optional.of(Flash.builder()
                        .type(ctx.sessionAttribute("flashType"))
                        .message(ctx.sessionAttribute("flash"))
                        .build()));

            ctx.sessionAttribute("flash", null);
            ctx.sessionAttribute("flashType", null);
        } else {
            map.put("flash", Optional.ofNullable(null));
        }

        return map;
    }
}
