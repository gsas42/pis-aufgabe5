package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.*;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

/**
 * Implements an in-memory database
 */
public class MemoryDatabase<E extends DatabaseObject> {
    private final ArrayList<E> data = new ArrayList<>();
    private int id = 0;

    /**
     * Gets the current ID and increments it
     *
     * @return id
     */
    public int nextId() {
        return id++;
    }

    /**
     * Calls fn for each data object
     *
     * @param fn the consuming lambda
     */
    public void forEach(Consumer<E> fn) {
        data.forEach(fn);
    }

    /**
     * Inserts a object into the data
     *
     * The object must have an ID that is not yet used
     *
     * @param e the object
     * @return e
     */
    public E insert(E e) {
        if(findBy(obj -> obj.getId() == e.getId()).isPresent()) {
            throw new IllegalArgumentException("ID has already been used!");
        }
        data.add(e);
        return e;
    }

    /**
     * Finds an object by a given predicate
     *
     * @param pred the predicate
     * @return an Optional<E>
     */
    public Optional<E> findBy(Predicate<E> pred) {
        return Optional.ofNullable(data.stream()
            .filter(pred)
            .findFirst()
            .orElse(null));
    }

    /**
     * Clears the entire database
     */
    public void clear() {
        data.clear();
        id = 0;
    }
}
