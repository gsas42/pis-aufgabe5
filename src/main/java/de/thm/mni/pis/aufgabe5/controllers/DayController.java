package de.thm.mni.pis.aufgabe5.controllers;

import de.thm.mni.pis.aufgabe5.*;
import de.thm.mni.pis.aufgabe5.models.*;
import de.thm.mni.pis.aufgabe5.services.*;
import de.thm.mni.pis.aufgabe5.controllers.ControllerUtil;

import java.util.*;
import io.javalin.Context;
import java.text.*;

public class DayController {
    /**
     * Gets or creates a day object depending on query parameters
     *
     * @param ctx the Javalin context
     * @param model the view model into which the resulting day and its date is stored
     * @return the day
     */
    private static Day ensureDay(Context ctx, Map<String, Object> model) {
        final var currentUser = (User)model.get("currentUser");

        final var date = Day.getDateFromFormattedString(ctx.queryParam("date"));
        final var day = DayService.getOrCreateDay(date, currentUser);
        model.put("day", day);

        final var dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        model.put("date", dateFormat.format(date));

        return day;
    }

    /**
     * #GET /day
     *
     * Shows the day-overview
     */
    public static void show(Context ctx) {
        final var model = ViewModel.of(ctx);
        ensureDay(ctx, model);

        model.put("siteTitle", "Kalorien");

        ctx.render("views/day/show.vm", model);
    }

    /**
     * #GET /add_food
     *
     * Shows the form for adding a food
     */
    public static void addFood(Context ctx) {
        final var model = ViewModel.of(ctx);
        ensureDay(ctx, model);
        model.put("siteTitle", "Essen hinzufügen");

        ctx.render("views/day/addFood.vm", model);
    }

    /**
     * #POST /add_food
     *
     * Creates a new food and adds it to the day
     */
    public static void createFood(Context ctx) {
        final var model = ViewModel.of(ctx);
        ensureDay(ctx, model);

        final var name = ctx.formParam("name", String.class).get();
        final var unit = ctx.formParam("unit", String.class).get();
        final int amount = ctx.formParam("amount", Integer.class).check(i -> i > 0).get();
        final int avgCalories = ctx.formParam("avgCalories", Integer.class).check(i -> i > 0).get();
        final int dayId = ctx.formParam("day", Integer.class).get();

        final var day = DayService.createFood(dayId, name, unit, amount, avgCalories);

        ControllerUtil.setFlash(ctx, "success", "Essen hinzugefügt!");

        final var dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ctx.redirect(Routes.DAY_SHOW + "?date=" + dateFormat.format(day.getDate()));
    }

    /**
     * #POST /remove_item
     *
     * Removes a food from a given day
     */
    public static void removeItem(Context ctx) {
        final var model = ViewModel.of(ctx);
        ensureDay(ctx, model);

        final var day = (Day)model.get("day");
        final int index = ctx.queryParam("idx", Integer.class).get();

        if(DayService.removeItem(day, index)) {
            ControllerUtil.setFlash(ctx, "success", "Item removed.");
            ctx.redirect(Routes.DAY_SHOW + "?date=" + ctx.queryParam("date"));
        } else {
            ctx.status(400);
        }
    }

    /**
     * #GET /suggest_item
     *
     * Finds a food item given a name
     */
    public static void suggestItem(Context ctx) {
        final var model = ViewModel.of(ctx);
        final var query = ctx.queryParam("q", String.class).get();

        final var food = DayService.findSimilarItem(query);

        if(food.isPresent()) {
            ctx.result(food.get().toJSON());
        } else {
            ctx.status(404);
        }
    }
}
