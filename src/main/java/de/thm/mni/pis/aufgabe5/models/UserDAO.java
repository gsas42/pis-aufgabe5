package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;

/**
 * The Data Access Object responsible for managing the user database
 */
public class UserDAO extends MemoryDatabase<User> {
    public static UserDAO instance = new UserDAO();
}
