package de.thm.mni.pis.aufgabe5.models;

/**
 * Common interface for things that affect total calories
 */
public interface EnergyExpenditure {
    /**
     * Returns a string that summarizes this expenditure
     */
    String getDescription();

    /**
     * Calculates the total energy expenditure, which can
     * be positive or negative
     *
     * @return amount of calories
     */
    int getCalories();

    /**
     * Used for views
     * Generates a string that looks like this:
     *
     * "+ 100 kcal"
     */
    default String getCaloriesString() {
        int calories = getCalories();
        return ((calories >= 0) ? "+ "+calories : "- "+calories) + " kcal";
    }
}
