package de.thm.mni.pis.aufgabe5;

/**
 * A static class that holds all routes
 */
public class Routes {
    public static final String SITE_INDEX = "/";

    public static final String DAY_SHOW = "/day";

    public static final String USER_REGISTER_POST = "/register";
    public static final String USER_LOGIN = "/login";
    public static final String USER_LOGIN_POST = "/login";

    public static final String USER_LOGOUT = "/logout";

    public static final String DAY_ADD_FOOD = "/add_food";
    public static final String DAY_ADD_FOOD_POST = "/add_food";

    public static final String DAY_REMOVE_ITEM = "/remove_item";

    public static final String DAY_SUGGEST_ITEM = "/suggest_item";
}
