package de.thm.mni.pis.aufgabe5;

import lombok.*;

/**
 * Represents a special message that is presented to the user after some
 * certain action, e.g. logging in successfully.
 */
@Builder
@Getter
public class Flash {
    /**
     * The type of flash
     */
    @Builder.Default private final String type = "info";

    /**
     * The flash message
     */
    private final String message;
}
