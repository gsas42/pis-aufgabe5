package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;
import java.text.*;
import java.time.LocalDate;
import java.util.function.*;
import lombok.*;

/**
 * Manages total energy expenditure for a single day
 */
@Getter
@Builder
public class Day implements DatabaseObject {
    /**
     * Determines the range in which a calorie goal is assumed to be reached
     */
    private static final int CALORIE_GOAL_TRESHOLD = 100;

    /**
     * The database ID
     */
    @Builder.Default public final int id = 0;

    /**
     * ID of the user that owns this resource
     */
    @Builder.Default public final int userId = 0;

    /**
     * The date on which this day occured
     */
    @Builder.Default public final Date date = new Date();

    /**
     * How many calories the user planned to ingest this day
     */
    public int plannedCalories;

    /**
     * A list of all energy expenditures that occured this day
     */
    @Builder.Default public final List<EnergyExpenditure> expenditures = new ArrayList<>();

    /**
     * Adds an expenditure to this day
     *
     * @param e an energy expenditure
     * @return this day
     */
    public Day addExpenditure(EnergyExpenditure e) {
        expenditures.add(e);
        return this;
    }

    /**
     * Returns this day's date as dd.MM.yyyy
     *
     * @return the date
     */
    public String getDateString() {
        // this should not belong here
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

    /**
     * Sums the calories of all expenditures
     *
     * @return the total amount of calories of today
     */
    public int getTotalCalories() {
        return expenditures.stream()
            .mapToInt(e -> e.getCalories())
            .sum();
    }

    /**
     * Gets the remaining calories of today
     *
     * @return the remaining calories of today
     */
    public int getRemainingCalories() {
        return plannedCalories - getTotalCalories();
    }

    /**
     * Returns an enum that judges the result of this day
     *
     * @return this day's result
     */
    public DayResult getResult() {
        int resultingCalories = getTotalCalories() - plannedCalories;

        /* In treshold? */
        if(Math.abs(resultingCalories) <= CALORIE_GOAL_TRESHOLD) {
            return DayResult.REACHED;
        }

        return (resultingCalories > 0) ? DayResult.TOO_MANY_CALORIES : DayResult.NOT_ENOUGH_CALORIES;
    }

    /**
     * Returns the date of tomorrow
     *
     * @return the date of tomorrow in yyyy-MM-dd
     */
    public String getNextDate() {
        return LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date)).plusDays(1).toString();
    }

    /**
     * Returns the date of yesterday
     *
     * @return the date of yesterday in yyyy-MM-dd
     */
    public String getPrevDate() {
        return LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date)).plusDays(-1).toString();
    }

    /**
     * Returns this day's date in yyy-MM-dd
     *
     * @return date in yyyy-MM-dd
     */
    public String getDateLocal() {
        return LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date)).toString();
    }

    /**
     * Returns a human-friendly version of getResult()
     *
     * @return String determining result of this day
     */
    public String getResultString() {
        switch(getResult()) {
            case REACHED: return "Ziel erreicht";
            case TOO_MANY_CALORIES: return "Zu viel!";
            case NOT_ENOUGH_CALORIES: return "Nicht ausreichend";
        }
        return "";
    }

    /**
     * Parses a date from a yyyy-MM-dd representation
     *
     * @param date the optional date
     * @return the date
     */
    public static Date getDateFromFormattedString(String date) {
        final var dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            return (date != null) ? dateFormat.parse(date) : new Date();
        } catch(ParseException e) {
            return new Date();
        }
    }

    /**
     * Checks if two dates are on the same day
     *
     * @param d1 the first date
     * @param d2 the second date
     * @return a boolean determining if both dates are on the same day
     */
    public static boolean onSameDay(Date d1, Date d2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(d1).equals(sdf.format(d2));
    }
}
