package de.thm.mni.pis.aufgabe5;

import de.thm.mni.pis.aufgabe5.*;
import de.thm.mni.pis.aufgabe5.controllers.*;
import de.thm.mni.pis.aufgabe5.models.*;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.security.SecurityUtil.roles;

/**
 * The main class
 */
public class App {
    public static void main(String[] args) {
        Javalin app = Javalin.create().enableStaticFiles("/public");

        /* Setup the access manager */
        app.accessManager((handler, ctx, permittedRoles) -> {
            UserRole userRole = ViewModel.getCurrentUser(ctx).isPresent() ? UserRole.LOGGED_IN : UserRole.NOT_LOGGED_IN;

            if (permittedRoles.contains(userRole)) {
                handler.handle(ctx);
            } else {
                ControllerUtil.setFlash(ctx, "danger", "Sie sind nicht eingeloggt!");
                ctx.redirect(Routes.SITE_INDEX);
            }
        });

        app.start(7000);

        /* Setup routes */
        app.routes(() -> {
            get(Routes.SITE_INDEX, SiteController::index, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

            get(Routes.DAY_SHOW, DayController::show, roles(UserRole.LOGGED_IN));

            post(Routes.USER_REGISTER_POST, UserController::createAccount, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

            get(Routes.USER_LOGIN, UserController::login, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));
            post(Routes.USER_LOGIN_POST, UserController::loginAccount, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

            get(Routes.USER_LOGOUT, UserController::logout, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));

            get(Routes.DAY_ADD_FOOD, DayController::addFood, roles(UserRole.LOGGED_IN));
            post(Routes.DAY_ADD_FOOD_POST, DayController::createFood, roles(UserRole.LOGGED_IN));

            get(Routes.DAY_REMOVE_ITEM, DayController::removeItem, roles(UserRole.LOGGED_IN));

            get(Routes.DAY_SUGGEST_ITEM, DayController::suggestItem, roles(UserRole.NOT_LOGGED_IN, UserRole.LOGGED_IN));
        });

        /* Create test account */
        UserDAO.instance.insert(User.builder().id(UserDAO.instance.nextId()).email("test@test.com").password("test123").build());
    }
}
