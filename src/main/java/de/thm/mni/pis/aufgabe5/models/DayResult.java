package de.thm.mni.pis.aufgabe5.models;

/**
 * An enum that judges the result of a day
 */
public enum DayResult {
    /**
     * The total amount of calories was within the treshold
     */
    REACHED,

    /**
     * The user consumed too few calories
     */
    NOT_ENOUGH_CALORIES,

    /**
     * The user consumed too many calories
     */
    TOO_MANY_CALORIES
}
