package de.thm.mni.pis.aufgabe5.services;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;

public class UserService {
    /**
     * Creates a new account
     *
     * @param email email
     * @param password password 
     * @param calorieGoal calorie goal
     *
     * @return the user account
     */
    public static User createAccount(String email, String password, int calorieGoal) {
        if(UserDAO.instance.findBy(u -> u.getEmail().equals(email)).isPresent()) {
            return null;
        }

        if(email == null || email.length() == 0) return null;
        if(password == null || password.length() == 0) return null;

        return UserDAO.instance
            .insert(User.builder()
                    .id(UserDAO.instance.nextId())
                    .email(email)
                    .password(password)
                    .calorieGoal(calorieGoal)
                    .build());
    }

    /**
     * Tries to find a user matching the email and password pair
     *
     * @param email email
     * @param password password
     *
     * @return an optional wrapping the user
     */
    public static Optional<User> loginAccount(String email, String password ) {
        return UserDAO.instance.findBy(u -> u.getEmail().equals(email) && u.getPassword().equals(password));
    }
}
