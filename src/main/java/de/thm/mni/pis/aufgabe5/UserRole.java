package de.thm.mni.pis.aufgabe5;

import io.javalin.security.Role;

/**
 * Roles used for authentication
 */
public enum UserRole implements Role {
    /**
     * Accessable even if not logged in
     */
    NOT_LOGGED_IN,

    /**
     * Only accessable with an account
     */
    LOGGED_IN
}
