package de.thm.mni.pis.aufgabe5.models;

import java.util.*;
import lombok.*;

/**
 * Implements the user
 */
@Getter
@Builder
public class User implements DatabaseObject {
    private final int id;
    @NonNull private final String email;
    @NonNull private final String password;

    @Builder.Default private final int calorieGoal = 2000;
}

