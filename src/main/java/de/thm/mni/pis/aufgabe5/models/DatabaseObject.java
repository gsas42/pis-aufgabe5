package de.thm.mni.pis.aufgabe5.models;

/**
 * Abstracts an object used for MemoryDatabase
 * that is insomuch useful that is allows to
 * check for IDs
 */
public interface DatabaseObject {
    /**
     * Gets the ID of this object
     *
     * @return the ID
     */
    int getId();
}
