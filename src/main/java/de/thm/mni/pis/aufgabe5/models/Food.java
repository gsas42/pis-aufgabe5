package de.thm.mni.pis.aufgabe5.models;

import de.thm.mni.pis.aufgabe5.models.EnergyExpenditure;

/**
 * Implements an abstract food type
 */
public class Food implements EnergyExpenditure {
    /**
     * Product name
     */
    public final String name;

    /**
     * The unit of the product, mostly either grams or litres
     */
    public final String unit;

    /**
     * How much of {unit}?
     */
    public final int amount;

    /**
     * How many calories per 100 of {unit}
     */
    public final int avgCalories;

    /**
     * Creates a new food object
     *
     * @param name name of this product
     * @param unit unit of this product
     * @param avgCalories how many calories are consumed per 100 of {unit}?
     * @param amount the amount
     * @throws IllegalArgumentException
     */
    public Food(String name, String unit, int avgCalories, int amount) {
        if(name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be empty");
        }

        this.name = name;

        if(unit == null || unit.length() == 0) {
            throw new IllegalArgumentException("Unit cannot be empty");
        }

        this.unit = unit;

        if(avgCalories < 0) {
            throw new IllegalArgumentException("Average calories cannot be negative");
        }

        this.avgCalories = avgCalories;

        if(amount < 0) {
            throw new IllegalArgumentException("Amount cannot be negative");
        }

        this.amount = amount;
    }

    /**
     * Gets a pretty string describing this food item
     *
     * @return description
     */
    public String getDescription() {
        return String.format("%s (%d kcal pro 100%s - %d%s)", name, avgCalories, unit, amount, unit);
    }

    /**
     * Calculates the calories consumed by this food
     *
     * @return calories
     */
    public int getCalories() {
        return avgCalories * amount / 100;
    }

    /**
     * Creates a JSON object describing this food item
     *
     * @return JSON object
     */
    public String toJSON() {
        return String.format("{ \"name\": \"%s\", \"unit\": \"%s\", \"amount\": %d, \"avgCalories\": %d }", name, unit, amount, avgCalories);
    }
}
