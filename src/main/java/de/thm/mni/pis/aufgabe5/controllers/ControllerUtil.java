package de.thm.mni.pis.aufgabe5.controllers;

import io.javalin.Context;

public class ControllerUtil {
    /**
     * Sets a flash message by changing session attributes
     *
     * @param ctx the Javalin context
     * @param type the flash type
     * @param message the flash message
     */
    public static void setFlash(Context ctx, String type, String message) {
        ctx.sessionAttribute("flashType", type);
        ctx.sessionAttribute("flash", message);
    }
}
