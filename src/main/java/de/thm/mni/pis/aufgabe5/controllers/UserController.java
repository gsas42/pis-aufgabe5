package de.thm.mni.pis.aufgabe5.controllers;

import de.thm.mni.pis.aufgabe5.models.*;
import de.thm.mni.pis.aufgabe5.*;
import de.thm.mni.pis.aufgabe5.controllers.ControllerUtil;
import de.thm.mni.pis.aufgabe5.services.*;

import java.util.*;
import io.javalin.Context;

public class UserController {
    /**
     * #POST /register
     *
     * Creates and logs into a new account
     */
    public static void createAccount(Context ctx) {
        final var email = ctx.formParam("email", String.class).get();
        final var password = ctx.formParam("password", String.class).get();
        final var calorieGoal = ctx.formParam("calorieGoal", Integer.class).check(i -> i > 0).get();

        final var u = UserService.createAccount(email, password, calorieGoal);
        if(u == null) {
            ControllerUtil.setFlash(ctx, "danger", "Konnte Account nicht erstellen!");
            ctx.redirect(Routes.SITE_INDEX);
        } else {
            ctx.cookieStore("currentUser", u.getId());

            ControllerUtil.setFlash(ctx, "success", "Benutzer wurde registriert!");

            ctx.redirect(Routes.DAY_SHOW);
        }
    }

    /**
     * #GET /login
     *
     * Serves the login page
     */
    public static void login(Context ctx) {
        final var model = ViewModel.of(ctx);
        model.put("hideNavBar", true);
        ctx.render("views/user/login.vm", model);
    }

    /**
     * #POST /login
     *
     * Logs into an existing user
     */
    public static void loginAccount(Context ctx) {
        final var email = ctx.formParam("email", String.class).get();
        final var password = ctx.formParam("password", String.class).get();

        final var user = UserService.loginAccount(email, password);
        if(user.isPresent()) {
            ControllerUtil.setFlash(ctx, "success", "Erfolgreich eingeloggt.");
            ctx.cookieStore("currentUser", user.get().getId());
            ctx.redirect(Routes.DAY_SHOW);
        } else {
            ControllerUtil.setFlash(ctx, "danger", "Kein Account gefunden.");
            ctx.redirect(Routes.USER_LOGIN);
        }
    }

    /**
     * #GET /logout
     *
     * Logs out the user
     */
    public static void logout(Context ctx) {
        ctx.clearCookieStore();
        ControllerUtil.setFlash(ctx, "success", "Erfolgreich ausgeloggt.");
        ctx.redirect(Routes.SITE_INDEX);
    }
}
