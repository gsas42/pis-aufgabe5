package de.thm.mni.pis.aufgabe5.controllers;

import de.thm.mni.pis.aufgabe5.models.*;

import java.util.*;
import io.javalin.Context;

public class SiteController {
    /**
     * Serves the index page
     *
     * @param ctx the Javalin context
     * @response 200 
     */
    public static void index(Context ctx) {
        final var model = ViewModel.of(ctx);
        model.put("siteTitle", "JFit");
        model.put("hideNavBar", true);
        ctx.render("views/site/index.vm", model);
    }
}
