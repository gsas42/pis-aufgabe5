package de.thm.mni.pis.aufgabe5.services;

import de.thm.mni.pis.aufgabe5.models.*;
import java.util.*;

public class DayService {
    /**
     * Get or creates a day object given a date and the current user
     *
     * @param date the date
     * @param currentUser the owning user
     * @return day
     */
    public static Day getOrCreateDay(Date date, User currentUser) {
        final var dayOpt = DayDAO.instance.findBy(d -> Day.onSameDay(d.getDate(), date) && d.getUserId() == currentUser.getId());
        return dayOpt.orElse(DayDAO.instance
                .insert(Day.builder()
                    .id(DayDAO.instance.nextId())
                    .date(date)
                    .plannedCalories(currentUser.getCalorieGoal())
                    .userId(currentUser.getId())
                    .build()));
    }

    /**
     * Creates a food item and adds it to the day
     *
     * @param dayId the ID of the day
     * @param name name
     * @param unit unit
     * @param amount amount
     * @param amount avgCalories
     *
     * @return the day
     */
    public static Day createFood(int dayId, String name, String unit, int amount, int avgCalories) {
        final var f = new Food(name, unit, avgCalories, amount);
        final var day = DayDAO.instance.findBy(d -> d.getId() == dayId);

        day.get().addExpenditure(f);
        return day.get();
    }

    /**
     * Removes an item from a day with the given index
     *
     * @param day the day
     * @param index the index
     * 
     * @return true when successful, else false
     */
    public static boolean removeItem(Day day, int index) {
        if(day.getExpenditures().size() > index) {
            day.getExpenditures().remove(index);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Tries to find a food with the given query name
     *
     * @param query the name of the food
     *
     * @return an optional wrapping this food
     */
    public static Optional<Food> findSimilarItem(String query) {
        final var list = new ArrayList<Food>();

        DayDAO.instance.forEach(d -> {
            d.expenditures.forEach(e -> {
                if(e instanceof Food) {
                    Food f = (Food)e;
                    if(f.name.equals(query)) {
                        list.add((Food)e);
                    }
                }
            });
        });

        if(list.size() > 0) {
            return Optional.of(list.get(0));
        } else {
            return Optional.ofNullable(null);
        }
    }
}
